import React from 'react';
import { Link } from 'react-router';
import CommentList from './CommentList';
import CommentInput from './CommentInput'

const CommentContainer = props => {
	if (props.currentUser) {
		return (
			<div className="col-xs-12 col-md-8 offset-md-2">
				<div>
					<list-errors errors={props.errors}></list-errors>
					<CommentInput slug={props.slug} currentUser={props.currentUser} />
				</div>

				<CommentList 
					comments={props.comments}
					slug={props.slug}
					currentUser={props.currentUser} />
			</div>
		)
	} else {
		return (
			<div className="col-xs-12 col-md-8 offset-md-2">
				<p>
					<Link to="login" >Sing In</Link>
					&nbsp; or&nbsp;
					<Link to="register" >Sing Up</Link>
					&nbsp;to add comment on this article.
				</p>

				<CommentList 
					comments={props.comments}
					slug={props.slug}
					currentUser={props.currentUser} />
			</div>
		)
	}
};

export default CommentContainer;